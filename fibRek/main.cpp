#include <iostream>

using namespace std;

int t, k;

long long Fibrek(int n)
{
    if(n <= 1) return 1;
    return Fibrek(n - 1) + Fibrek(n - 2);
}

int main()
{   
    cin >> t;
    while (t--)
    { 
      cin >> k;
      cout << Fibrek(k) << "\n";
    }
}