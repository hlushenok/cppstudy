#include <iostream>

using namespace std;

int main() {
    int elementsSize;
    cin >> elementsSize;
    int elements[elementsSize];
    for (int i = 0; i < elementsSize; ++i) {
        cin >> elements[i];
    }

    int conditionsSize;
    cin >> conditionsSize;
    int conditions[conditionsSize];
    for (int j = 0; j < conditionsSize; ++j) {
        cin >> conditions[j];
    }

    int a = 0, k = 0;
    do {
        if (elements[k] >= conditions[a]) {
                cout << elementsSize - k << endl;
                a++;
                k = 0;
            }else{
                k++;
            }

        if (a == conditionsSize -1 && k == 0 && elements[k] < conditions[a]){
            cout << k << endl;
        }
    }while (a < conditionsSize - 1);

    return 0;
}