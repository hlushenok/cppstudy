#include <iostream>

using namespace std;

int main() {
    int x;
    cin >> x;
    int el = x;
    for (int i=2; i*i<=x; ++i) {
        if (x % i == 0) el -= el / i;
        while (x % i == 0){
            x /= i;
        }
    }
    if (x > 1) el -= el / x;
    cout << el;
    return 0;
}