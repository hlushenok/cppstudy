#include <iostream>

using namespace std;

int main()
{
    int a, b;
    cin >> a >> b;
    int i = 0;
    while ( b > 1)
    {
        b /= a;
        i++;
    }
    cout << i << endl;
    return 0;
}
