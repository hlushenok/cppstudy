#include <iostream>
#include <cmath>
using namespace std;

int main() {
    int x;
    cin >> x;
    if (x != 0 && x<10000){
        if (x<0) x *= -1;
        cout << int(sqrt(x));
    }
    return 0;
}