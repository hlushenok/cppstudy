#include <iostream>
using namespace std;


int main(){
    const int N = 47;
    int liczby[N] = {0,1};
    for (int i = 2; i < N; ++i) {
        liczby[i] = liczby[i-1] + liczby[i-2];
    }
    int n;
    cin >> n;
    while (n > 0){

        int ost = 0;
        int poz = 2;
        int wyn = 0;
        char c;
        for (cin >> c, n--;!(ost == 1 && c == '1'); cin >> c, n--) {
            ost = (c == '1');
            wyn += liczby[poz++] * ost;
        }
        cout << wyn << endl;
    }
    return 0;
}
