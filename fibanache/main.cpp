#include <iostream>
using namespace std;

int Fib(int k){
    if(k <= 0) return 1;
    if(k <= 1) return 1;
    return Fib(k - 1) + Fib(k - 2);
}

int main(){
    int enter,answ;
    cin >> enter;
    for (int i = 0; i < enter+1; ++i) {
        answ =  Fib(i);
    }
    cout << answ << endl;
    return 0;
}
