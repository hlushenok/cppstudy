#include <iostream>

using namespace std;

int selectSize(int x,int y,int del){
    int delPart;

    if (x % del != 0)return selectSize(x,y,del+1);

    delPart = x / del;

    if (y % delPart != 0)return selectSize(x,y,del+1);

    return delPart;
}

int main() {
    int x,y;
    cin >> x >> y;
    if (x >= 1 && x <= 1000000 && y >= 1 && y <= 1000000)cout << selectSize(x,y,2) ;
}