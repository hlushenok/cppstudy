#include <iostream>

using namespace std;

int main() {
    int x;
    cin >> x;
    int numbers[x];
    for (int i = 0; i < x; ++i) {
        cin >> numbers[i];
    }
    long startTime = clock();
    for (int l = 0; l < x; ++l) {
        for (int j = x - 1; j > 0; --j) {
            int k = numbers[j - 1] - numbers[j];
            if (k > 0) {
                numbers[j - 1] = numbers[j - 1] - k;
                numbers[j] = numbers[j] + k;
            }
        }
    }
    long stopTime = clock();
    for (int k = 0; k < x; ++k) {
        cout << numbers[k] <<" ";
    }

    cout << endl;
    cout << (stopTime - startTime);
    return 0;
}