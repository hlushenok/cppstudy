#include <iostream>

using namespace std;

int main() {
    int x;
    cin >> x;
    int k[x];
    for (int i = 0; i < x; ++i) {
        cin >> k[i];
    }
    int max = k[0];
    for (int j = 0; j < x; ++j) {
        if (k[j] > max){
            max = k[j];
        }
        cout << max << "\n";
    }

    return 0;
}