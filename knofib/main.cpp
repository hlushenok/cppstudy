#include <cstdio>
using namespace std;

int k,c;

int fib(int n){
    c++;
    if (c > k) return 0;
    printf("%d\n",n);
    if (c == k) return 0;
    if (n < 2) return n;
    fib(n - 1);
    fib(n - 2);
}

int main() {
    int n;

    scanf("%d %d",&n,&k);

    if (n < 100000 ) fib(n);
    return 0;
}