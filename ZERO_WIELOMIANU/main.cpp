#include <iostream>
#include <iomanip>

using namespace std;

double wartosc(double x,const double p[],unsigned n)
{
    double sum;
    for(sum=0;n--;sum+=p[n]) sum*=x;
    return sum;
}


int main() {
    int n;
    cin >> n;
    double table[n] = {0,0,0,0,0};
    for (int i = 0; i < n; ++i) cin >> table[i];

    double wynik, a = 0, b = 100;
    double m = 0;
    while (b - a >= 1e-8){
        m = (b - a) / 2;
        wynik = wartosc(m,table,n);
        //cout << m << " " << n << " " << endl;
        if (wynik == 0.0) break;
        if (wynik > 0) a = m;
        else b = m;
    }
    //for (int j = 0; j < n; ++j) cout << table[j];
    cout << showpoint << fixed << setprecision(7) << m << endl;
    return 0;
}