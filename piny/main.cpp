#include <iostream>

using namespace std;

int main() {
    int x = 0;
    cin >> x;
    if (x < 2) cout << 1;
    else{
        long long  tab[x+1];
        tab[0] = tab[1] = 1;
        for (int i = 2; i <= x; ++i) tab[i] = tab[i-1] + tab[i-2];
        cout << tab[x] << endl;
    }
    return 0;
}