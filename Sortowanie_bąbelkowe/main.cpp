#include <iostream>

using namespace std;

int main() {
    int x;
    bool flag = true;
    cin >> x;
    int array[x];
    for (int i = 0; i < x; ++i) {
        cin >> array[i];
    }
    do {
        flag = false;
        for (int j = 1; j < (sizeof(array) / 4); ++j) {
            if (array[j] < array[j - 1]) {
                swap(array[j], array[j - 1]);
                flag = true;
            }
        }
        if (flag) {
            for (int j = 0; j < (sizeof(array) / 4); ++j) {
                cout << array[j] << " ";
            }
            cout << endl;
        }
    } while (flag);

    return 0;
}