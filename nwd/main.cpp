#include <iostream>

using namespace std;


int main() {
    int x;
    bool flag;
    cin >> x;
    int numbers[x];
    for (int i = 0; i < x; ++i) {
        cin >> numbers[i];
    }
    int min = numbers[0];
    for (int j = 0; j < x; ++j) {
        if (numbers[j] < min){
            min = numbers[j];
        }

    }
    do {
        flag = false;
        for (int k = 0; k < x; ++k) {
            if (numbers[k] % min != 0){
                min--;
                flag = true;
            }
        }
    }while (flag);

    cout << min << endl;
    return 0;
}