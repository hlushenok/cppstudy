cmake_minimum_required(VERSION 3.8)
project(silnia2)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES main.cpp)
add_executable(silnia2 ${SOURCE_FILES})